<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>

<div class="updated easy-booking-notice">
	<p>
		<?php printf( __( 'Thanks for updating Easy Booking! This update contains important changes, please %smake sure your settings are correctly saved.%s', 'woocommerce-easy-booking-system' ), '<a href="admin.php?page=easy-booking">', '</a>' ); ?>
	</p>
	<p><b>
		<?php _e( 'To match wordpress.org internationalization rules, the plugin\'s textdomain has been modified.', 'woocommerce-easy-booking-system' ); ?>
	</b></p>
	<p>
		<?php _e( 'If you have .mo and .po files, please rename them this way to get your translations back:', 'woocommerce-easy-booking-system' ); ?>
		<ul>
			<li><strike>(easy_booking-fr_FR.po)</strike> <b>woocommerce-easy-booking-system-fr_FR.po</b></li>
			<li><strike>(easy_booking-fr_FR.mo)</strike> <b>woocommerce-easy-booking-system-fr_FR.mo</b></li>
		</ul>

		<?php _e( 'Replace "fr_FR" by your own language code.', 'woocommerce-easy-booking-system' ); ?>
	</p>
</div>