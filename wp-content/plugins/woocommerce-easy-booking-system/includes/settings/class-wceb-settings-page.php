<?php

class WCEB_Settings_Page {
	
	public function __construct() {

		add_action( 'admin_menu', array( $this, 'add_settings_page' ), 10 );

		if ( is_multisite() ) {
			add_action( 'network_admin_menu', array( $this,'add_network_settings_page' ) );
		}

	}

	/**
	 *
	 * Add settings page into "Easy Booking" menu.
	 *
	 */
	public function add_settings_page() {

		// Create a "Settings" page inside "Easy Booking" menu
		$settings_page = add_submenu_page(
			'easy-booking',
			__( 'Settings', 'woocommerce-easy-booking-system' ),
			__( 'Settings', 'woocommerce-easy-booking-system' ),
			apply_filters( 'easy_booking_settings_capability', 'manage_options' ),
			'easy-booking',
			array( $this, 'display_settings_page' )
		);

		// Maybe load scripts on the "Settings" page.
		add_action( 'admin_print_scripts-'. $settings_page, array( $this, 'load_settings_scripts' ) );

	}

	/**
	 *
	 * Add network settings page into "Easy Booking" menu for multisites.
	 *
	 */
	public function add_network_settings_page() {

		// Create a "Network" page inside "Easy Booking" menu for multisites
		$option_page = add_submenu_page(
			'easy-booking',
			__( 'Network Settings', 'woocommerce-easy-booking-system' ),
			__( 'Network Settings', 'woocommerce-easy-booking-system' ),
			apply_filters( 'easy_booking_settings_capability', 'manage_options' ),
			'easy-booking',
			array( $this, 'display_network_settings_page' )
		);

	}

	/**
	 *
	 * Load HTML for settings page.
	 *
	 */
	public function display_settings_page() {
		include_once( 'views/html-wceb-settings-page.php' );
	}

	/**
	 *
	 * Load HTML for network settings page.
	 *
	 */
	public function display_network_settings_page() {
		include_once( 'views/html-wceb-network-settings-page.php' );
	}

	/**
	 *
	 * Load CSS and JS for settings page.
	 *
	 */
	public function load_settings_scripts() {

		// WP colorpicker CSS.
		wp_enqueue_style( 'wp-color-picker' );

		// WP colorpicker JS.
	  	wp_enqueue_script(
	  		'color-picker',
	  		plugins_url( 'assets/js/admin/colorpicker.min.js', WCEB_PLUGIN_FILE ),
	  		array( 'wp-color-picker' ),
	  		false,
	  		true
	  	);

	}

}

return new WCEB_Settings_Page();